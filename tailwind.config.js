/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: "#5552FF",
        ['primary-lighter']: "#6B68FF",
        red: "#E70000",
      },
    },
  },
  plugins: [],
}
