import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createVfm } from 'vue-final-modal'
import 'vue-final-modal/style.css'  

import App from './App.vue'
import router from './router'
import Notifications from 'notiwind'
import { useAuthStore } from './stores/auth.store'

import './assets/css/main.css'

const app = createApp(App)

app.use(createPinia())

const authStore = useAuthStore()
const vfm = createVfm()


authStore.tryReLogin().finally(() => {
  app.use(Notifications).use(vfm).use(router).mount('#app')
})
