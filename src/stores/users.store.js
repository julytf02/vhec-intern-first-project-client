import { defineStore } from 'pinia'
import axios from 'axios'
import usersService from '@/services/users.service'

export const useUsersStore = defineStore('user', {
  state: () => ({
    users: [],
    currentUser: null,
    selectedUsers: [],
    pagination: {
      page: 1,
      perPage: 17,
    },
    query: {
      sortBy: null,
      order: 'asc',
      filter: {
        id: null,
        name: null,
        email: null,
        phoneNumber: null,
        address: null,
      },
    },
    metadata: {
      totalPages: 0,
      totalItems: 0,
    },
  }),
  getters: {
    usersCount() {
      return this.users.length
    },
  },

  actions: {
    async getUsers() {
      try {
        const response = await usersService.getUsers({
          page: this.pagination.page,
          perPage: this.pagination.perPage,
          sortBy: this.query.sortBy,
          order: this.query.order,
          filter: this.query.filter,
        })
        this.users = formatUsersFromServer(response.data)
        this.selectedUsers = []
        // this.pagination = {
        //   page: response.pagination.page,
        //   perPage: response.pagination.perPage,
        // }
        this.metadata = {
          totalPages: response.pagination.totalPages,
          totalItems: response.pagination.totalItems,
        }
        return null
      } catch (err) {
        return err?.response?.data || { status: 400, message: 'An error occurred' }
      } finally {
      }
    },
    async createUser(user) {
      try {
        const response = await usersService.createUser(user)
        // await this.fetchUsers()
        return null
      } catch (err) {
        return err?.response?.data || { status: 400, message: 'An error occurred' }
      } finally {
      }
    },
    async updateUser(userId, user) {
      try {
        const response = await usersService.updateUser(userId, user)
        // await this.fetchUsers()
        return null
      } catch (err) {
        return err?.response?.data || { status: 400, message: 'An error occurred' }
      } finally {
      }
    },
    async deleteUser(userId) {
      try {
        const data = await usersService.deleteUser(userId)
        if (this.users.length === 1 && this.pagination.page > 1) {
          this.pagination.page -= 1
        }
        // await this.fetchUsers()
        return null
      } catch (err) {
        console.log('😍', err)
        return err?.response?.data || { status: 400, message: 'An error occurred' }
      } finally {
      }
    },
    async deleteSelectedUsers() {
      try {
        const selectedUserIds = this.selectedUsers.map((user) => user.id)
        await Promise.all(selectedUserIds.map((userId) => usersService.deleteUser(userId)))

        if (this.users.length === this.selectedUsers.length && this.pagination.page > 1) {
          this.pagination.page -= 1
        }
        // await this.fetchUsers()
        return null
      } catch (err) {
        return err?.response?.data || { status: 400, message: 'An error occurred' }
      } finally {
      }
    },
  },
})

function formatUsersFromServer(users) {
  return users.map((user) => formatUserFromServer(user))
}

function formatUserFromServer(user) {
  return {
    id: user.Id,
    name: user.Name,
    email: user.Email,
    phoneNumber: user.PhoneNumber,
    address: user.Address,
  }
}
