import router from '@/router'
import authService from '@/services/auth.service'
import { defineStore } from 'pinia'

export const useAuthStore = defineStore('auth', {
  state: () => {
    return { token: null, user: null, isLoggedIn: false }
  },
  actions: {
    async tryReLogin() {
      const oldToken = localStorage.getItem('token')

      if (!oldToken) {
        return false
      }

      this.token = oldToken

      try {
        const data = await authService.reLogin()
        const { user } = data.data
        this.user = {
          id: user.Id,
          email: user.Email,
          name: user.Name,
          phoneNumber: user.PhoneNumber,
          address: user.Address,
        }
        this.isLoggedIn = true
        
        return true
      } catch (e) {
        console.log(e)
        this.token = null
        localStorage.removeItem('token')
        return false
      }
    },
    async register(email, password) {
      try {
        const data = await authService.register(email, password)
        const { user, token } = data.data
        this.token = token
        this.user = {
          id: user.Id,
          email: user.Email,
          name: user.Name,
          phoneNumber: user.PhoneNumber,
          address: user.Address,
        }
        this.isLoggedIn = true
        localStorage.setItem('token', token)
        return null
      } catch (e) {
        console.log(e)
        return e?.response?.data || { status: 400, message: 'An error occurred' }
      }
    },
    async login(email, password) {
      try {
        const data = await authService.login(email, password)
        const { user, token } = data.data
        this.token = token
        this.user = {
          id: user.Id,
          email: user.Email,
          name: user.Name,
          phoneNumber: user.PhoneNumber,
          address: user.Address,
        }
        this.isLoggedIn = true
        localStorage.setItem('token', token)
        return null
      } catch (e) {
        console.log(e)
        return e?.response?.data || { status: 400, message: 'An error occurred' }
      }
    },
    async logout() {
      this.token = null
      this.user = null
      this.isLoggedIn = false
      localStorage.removeItem('token')
      router.push({ name: 'login' })
    },
    async checkEmail(email) {
      try {
        await authService.checkEmail(email)
        return true
      } catch (e) {
        console.log(e)
        return false
      }
    },
  },
})
