import axios from 'axios'
import axiosClient from '@/services/api/axios.service'
import { useAuthStore } from '@/stores/auth.store'

class AuthService {
  async reLogin() {
    const authStore = useAuthStore()

    const response = await axiosClient.post(
      '/api/auth/relogin',
      {},
      {
        headers: { Authorization: `Bearer ${authStore.token}` },
      }
    )
    return response.data
  }

  async login(email, password) {
    const response = await axiosClient.post('/api/auth/login', {
      email: email,
      password: password,
    })

    return response.data
  }

  async register(email, password) {
    const response = await axiosClient.post('/api/auth/register', {
      email: email,
      password: password,
    })

    return response.data
  }

  //   async logout() {
  //     localStorage.removeItem('token')
  //   }

  async checkEmail(email) {
    const response = await axiosClient.get('/api/auth/check-email', {
      params: {
        email: email,
      },
    })

    return response.data
  }
}

export default new AuthService()
