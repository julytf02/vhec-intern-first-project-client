import axios from 'axios'
import axiosClient from '@/services/api/axios.service'
import { useAuthStore } from '@/stores/auth.store'

class UserService {
  async getUsers({ page = 1, perPage = 24, sortBy = null, order = 'asc', filter = {} }) {
    const authStore = useAuthStore()
// console.log(sortBy)
    const response = await axiosClient.get('/api/user', {
      headers: { Authorization: `Bearer ${authStore.token}` },
      params: { page, perPage, sortBy, order, ...filter },
    })
    return response.data
  }

  async createUser(user) {
    const authStore = useAuthStore()

    const response = await axiosClient.post('/api/user', user, {
      headers: { Authorization: `Bearer ${authStore.token}` },
    })
    return response.data
  }

  async updateUser(userId, updatedUser) {
    const authStore = useAuthStore()

    const response = await axiosClient.put(`/api/user/${userId}`, updatedUser, {
      headers: { Authorization: `Bearer ${authStore.token}` },
    })
    return response.data
  }

  async deleteUser(userId) {
    const authStore = useAuthStore()

    const response = await axiosClient.delete(`/api/user/${userId}`, {
      headers: { Authorization: `Bearer ${authStore.token}` },
    })

    return response.data
  }

}

export default new UserService()
